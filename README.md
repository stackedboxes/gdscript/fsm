# Finite-State Machine (FSM)

*Part of [StackedBoxes' GDScript Hodgepodge](https://gitlab.com/stackedboxes/gdscript/)*

Finite-State Machines for implementing character behaviors.

This is strongly based on the nice [FSM
code](https://github.com/GDQuest/godot-design-patterns/tree/main/godot/StateMachine)
and
[tutorial](https://www.gdquest.com/tutorial/godot/design-patterns/finite-state-machine/)
by GDQuest.
