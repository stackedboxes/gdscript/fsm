#
# Finite-State Machines (FSM)
# StackedBoxes' GDScript Hodgepodge
#
# Copyright (c) 2020 GDQuest
# Copyright (c) 2021 Leandro Motta Barros
#

extends Node
class_name SbxsFSMState

# The state machine that "owns" this state. The FSM itself will set this field.
var _fsm = null

# Is this the currently active FSM state? Set by the FSM itself.
#
# This is handy when you have an event handler on the FSM state and you only
# want to run this event handler if the state is the current one. The thing is,
# all FSM states, even if inactive (not current), are always part of the scene
# tree and therefore always handle events. If you want to handle the events only
# if the state is the current one, checking or this variable is the easiest way
# to go. (Alternatively, you can connect and disconnect the events on
# `onEnter()` and `onExit()`.)
var _isCurrent := false

# The FSM will call this on its _ready() function, at a point in time we know
# that the node that owns this FSM is already initialized. Because of this
# initialization-of-the-owner guarantee, concrete FSM state implementations are
# advisable to override this method instead of _ready().
func onReady() -> void:
	pass


# States can override this method to implement the desired `_unhandled_input()`
# behavior when they are the current state.
func handleUnhandledInput(_event: InputEvent) -> void:
	pass


# States can override this method to implement the desired `_process()` behavior
# when they are the current state.
func handleProcess(_delta: float) -> void:
	pass


# States can override this method to implement the desired `_physics_process`
# behavior when they are the current state.
func handlePhysicsProcess(_delta: float) -> void:
	pass


# Called by the FSM when this becomes the current state. `arg` is an arbitrary
# value passed by whoever asked for the state transition.
func onEnter(_arg = null) -> void:
	pass


# Called by the FSM when this ceases to be the current state.
func onExit() -> void:
	pass
