#
# Finite-State Machines (FSM)
# StackedBoxes' GDScript Hodgepodge
#
# Copyright (c) 2020 GDQuest
# Copyright (c) 2021 Leandro Motta Barros
#

extends Node
class_name SbxsFSM

# Emitted when transitioning to a new state. Also emitted when the state machine
# starts -- in which case `oldState` will be `null`.
signal transitioned(oldState, newState)

# Path to the initial state.
export var _initialState := NodePath()

# The current active state.
onready var _currentState: SbxsFSMState = get_node(_initialState)


func _ready() -> void:
	yield(owner, "ready")

	# Set itself as the FMS that "owns" the child states.
	for child in get_children():
		child._fsm = self
		child.onReady()

	# Let the world know what the initial state is active.
	_currentState._isCurrent = true
	_currentState.onEnter()
	emit_signal("transitioned", null, _currentState.name)


# Forward _unhandled_input to the current state.
func _unhandled_input(event: InputEvent) -> void:
	_currentState.handleUnhandledInput(event)


# Forward _process to the current state.
func _process(delta: float) -> void:
	_currentState.handleProcess(delta)


# Forward _physics_process to the current state.
func _physics_process(delta: float) -> void:
	_currentState.handlePhysicsProcess(delta)


# Transitions to the state named `targetStateName`. If not such state exists, an
# `assert()`ion will fail. `arg` is an arbitrary value passed to the target
# state's `onEnter()` method.
func transitionTo(targetStateName: String, arg = null) -> void:
	var oldStateName = _currentState.name
	_currentState._isCurrent = false
	_currentState.onExit()
	_currentState = get_node(targetStateName)
	assert(_currentState != null)
	_currentState._isCurrent = true
	_currentState.onEnter(arg)
	emit_signal("transitioned", oldStateName, _currentState.name)
